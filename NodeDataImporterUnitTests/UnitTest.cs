﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NodeDataImporter;
using System.Xml.Linq;

namespace NodeDataImporterUnitTests
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "NodeFactory, CreateFrom XML Element: XML Element must be provided")]
        public void DoesNotAllowNull()
        {
            NodeFactory.CreateFrom(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "NodeFactory, CreateFrom XML Element: XML Element is expected to be at Node level")]
        public void DoesNotAllowIncorectRoot()
        {
            var xDoc = XDocument.Parse(@"<test></test>");
            NodeFactory.CreateFrom(xDoc.Root);
        }

        [TestMethod]
        public void CheckForID()
        {
            var xDoc = XDocument.Parse(@"
                <node>
                    <label>Test</label>
                    <adjacentNodes />
                </node>");
            var node = NodeFactory.CreateFrom(xDoc.Root);

            Assert.IsNull(node);
        }

        [TestMethod]
        public void CheckIDIsInt()
        {
            var xDoc = XDocument.Parse(@"
                <node>
                    <id>ERROR</id>
                    <label>Test</label>
                    <adjacentNodes />
                </node>");
            var node = NodeFactory.CreateFrom(xDoc.Root);

            Assert.IsNull(node);
        }

        [TestMethod]
        public void CheckForLabel()
        {
            var xDoc = XDocument.Parse(@"
                <node>
                    <id>1</id>
                    <adjacentNodes />
                </node>");
            var node = NodeFactory.CreateFrom(xDoc.Root);

            Assert.IsNull(node);
        }

        [TestMethod]
        public void CheckNodeCreationWithOneAdjacentNodes()
        {
            var xDoc = XDocument.Parse(@"
                <node>
                    <id>1</id>
                    <label>Test</label>
                    <adjacentNodes>
                        <id>2</id>
                    </adjacentNodes>
                </node>");
            var node = NodeFactory.CreateFrom(xDoc.Root);

            Assert.IsNotNull(node);
            Assert.AreEqual(node.ID, 1);
            Assert.AreEqual(node.Label, "Test");
            Assert.IsNotNull(node.AdjacentNodes);
            Assert.AreEqual(node.AdjacentNodes.Length, 1);
            Assert.AreEqual(node.AdjacentNodes[0], 2);
        }

        [TestMethod]
        public void CheckNodeCreationWithTwoAdjacentNodes()
        {
            var xDoc = XDocument.Parse(@"
                <node>
                    <id>1</id>
                    <label>Test</label>
                    <adjacentNodes>
                        <id>2</id>
                        <id>3</id>
                    </adjacentNodes>
                </node>");
            var node = NodeFactory.CreateFrom(xDoc.Root);

            Assert.IsNotNull(node);
            Assert.AreEqual(node.ID, 1);
            Assert.AreEqual(node.Label, "Test");
            Assert.IsNotNull(node.AdjacentNodes);
            Assert.AreEqual(node.AdjacentNodes.Length, 2);
            Assert.AreEqual(node.AdjacentNodes[0], 2);
            Assert.AreEqual(node.AdjacentNodes[1], 3);
        }

        [TestMethod]
        public void CheckNodeCreationWithAdjacentNodesAsString()
        {
            var xDoc = XDocument.Parse(@"
                <node>
                    <id>1</id>
                    <label>Test</label>
                    <adjacentNodes>
                        <id>2</id>
                        <id>WRONG</id>
                    </adjacentNodes>
                </node>");
            var node = NodeFactory.CreateFrom(xDoc.Root);

            Assert.IsNull(node);
        }
    }
}
