﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace MongoWrapperHelper
{
    public class MongoWrapper
    {
        public MongoClient Client { get; private set; }

        private Dictionary<string, IMongoDatabase> mongoDatabases;
        public MongoWrapper()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["MongoDBConnectionString"]?.ConnectionString;

            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ConfigurationErrorsException("Unable to load 'MongoDBConnectionString' from config file");

            try
            {
                Client = new MongoClient(connectionString);
            }
            catch (Exception e)
            {
                throw new MongoWrapperException(string.Format("Unable to connect to Mongo DB with connection string '{0}'. Exception: {1}", connectionString, e.Message, e));
            }

            mongoDatabases = new Dictionary<string, IMongoDatabase>();
        }

        public IMongoDatabase this[string dataBase]
        {
            get
            {
                return GetDatabase(dataBase);
            }
        }

        public IMongoDatabase GetDatabase(string dataBase)
        {
            if (!mongoDatabases.ContainsKey(dataBase))
            {
                try
                {
                    mongoDatabases.Add(dataBase, Client.GetDatabase(dataBase));
                }
                catch (Exception e)
                {
                    throw new MongoWrapperException(string.Format("Unable to get DataBase '{0}' from Mongo. Exception: {1}", dataBase, e.Message), e);
                }
            }

            return mongoDatabases[dataBase];
        }

        public IMongoCollection<T> GetCollection<T>(string dataBase, string collection)
        {
            return this[dataBase].GetCollection<T>(collection);
        }
    }
}
