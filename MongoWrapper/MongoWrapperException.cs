﻿using System;

namespace MongoWrapperHelper
{
    public class MongoWrapperException : Exception
    {
        public override string Message { get; }
        //public override Exception InnerException { get; }

        public MongoWrapperException(string message = null, Exception innerException = null)
        : base(message, innerException)
        {
        }
    }
}