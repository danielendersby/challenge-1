﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using MongoWrapperHelper;
using NodeService.Other;
using MongoDB.Driver;
using System.Configuration;
using NodeService.DataManagerBackend;

namespace NodeService
{
    public class DataManager : IDataManager
    {
        //Inset or Update
        public bool UpsertNode(Node node)
        {
            if (node == null)
                throw new ArgumentException("No Node provided");

            //TODO: Implement Authentication

            var upserter = new Upserter();
            return upserter.UpsertNode(node);
        }

        public bool UpsertNodes(IEnumerable<Node> nodes)
        {
            if (nodes == null)
                throw new ArgumentException("No Nodes provided");

            //TODO: Implement Authentication

            var upserter = new Upserter();
            return upserter.UpsertNodes(nodes);
        }

        //Trim
        public bool TrimUnreferencedNodes(IEnumerable<int> ids)
        {
            if (ids == null)
                throw new Exception("No IDs provided");

            //TODO: Implement Authentication

            var trimmer = new Trimmer();
            return trimmer.TrimUnreferencedNodes(ids);
        }

        //Remove

        public bool RemoveNode(Node node)
        {
            if (node == null)
                throw new ArgumentException("No Node provided");

            //TODO: Implement Authentication

            var remover = new Remover();
            return remover.RemoveNode(node);
        }
        public bool RemoveNode(int? id)
        {
            if (!id.HasValue)
                throw new ArgumentException("No Node ID provided");

            //TODO: Implement Authentication

            var remover = new Remover();
            return remover.RemoveNode(id.Value);
        }

        public bool RemoveNodes(IEnumerable<int> ids)
        {
            if (ids == null)
                throw new Exception("No Node IDs provided");

            //TODO: Implement Authentication

            var remover = new Remover();
            return remover.RemoveNodes(ids);
        }

        //Get
        public IEnumerable<Node> GetAllNodes()
        {
            //TODO: Implement Authentication

            var getter = new Getter();
            return getter.GetAllNodes();
        }

        public Node GetNodeWith(int? id)
        {
            if (!id.HasValue)
                throw new ArgumentException("No Node ID provided");

            //TODO: Implement Authentication

            var getter = new Getter();
            return getter.GetNodeWith(id.Value);
        }

        public IEnumerable<Node> GetAllNodesWith(IEnumerable<int> ids)
        {
            if (ids == null)
                throw new ArgumentException("No Node IDs provided");

            //TODO: Implement Authentication

            var getter = new Getter();
            return getter.GetAllNodesWith(ids);
        }
    }
}
