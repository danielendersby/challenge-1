﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NodeService
{
    [ServiceContract]
    public interface IDataManager
    {
        //Inset or Update
        [OperationContract]
        bool UpsertNode(Node node);

        [OperationContract]
        bool UpsertNodes(IEnumerable<Node> nodes);

        //Trim
        [OperationContract]
        bool TrimUnreferencedNodes(IEnumerable<int> ids);

        //Remove
        [OperationContract]
        bool RemoveNode(int? id);

        [OperationContract]
        bool RemoveNodes(IEnumerable<int> ids);

        //Get
        [OperationContract]
        IEnumerable<Node> GetAllNodes();

        [OperationContract]
        Node GetNodeWith(int? id);

        [OperationContract]
        IEnumerable<Node> GetAllNodesWith(IEnumerable<int> ids = null);
    }

    [DataContract]
    public class Node
    {
        [BsonId]
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Label { get; set; }

        [DataMember]
        public int[] AdjacentNodes { get; set; }
    }
}
