﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace NodeService.Other
{
    public static class ConfigHelper
    {
        public static string NodeDatabase
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["NodeDataBase"];
                }
                catch //(Exception e)
                {
                    throw new ConfigurationErrorsException("Unable to load 'NodeDataBase' from config file");
                }
            }
        }

        public static string NodeCollection
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["NodeCollection"];
                }
                catch //(Exception e)
                {
                    throw new ConfigurationErrorsException("Unable to load 'NodeCollection' from config file");
                }
            }
        }
    }
}