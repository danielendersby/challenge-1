﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoWrapperHelper;
using NodeService.Other;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NodeService.AdminActionsBackend
{
    public class Dropper
    {
        private MongoWrapper mongoWrapper;

        public Dropper()
        {
            mongoWrapper = new MongoWrapper();
        }

        public bool DropDataBase()
        {
            try
            {
                mongoWrapper.Client.DropDatabase(ConfigHelper.NodeDatabase);
            }
            catch (ConfigurationErrorsException e)
            {
                throw e;
            }
            catch (MongoWrapperException e)
            {
                throw e;
            }
            catch (MongoException e)
            {
                throw e;
            }
            catch //(Exception e) Unknown Exception not exposed to user for security
            {
                throw new Exception("Unknwon Error occored Dropping Database");
            }

            return true;
        }

        public bool DropCollection()
        {
            try
            {
                mongoWrapper[ConfigHelper.NodeDatabase].DropCollection(ConfigHelper.NodeCollection);
            }
            catch (ConfigurationErrorsException e)
            {
                throw e;
            }
            catch (MongoWrapperException e)
            {
                throw e;
            }
            catch (MongoException e)
            {
                throw e;
            }
            catch //(Exception e) Unknown Exception not exposed to user for security
            {
                throw new Exception("Unknwon Error occored Dropping Collection");
            }

            return true;
        }
    }
}