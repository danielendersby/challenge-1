﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NodeService
{
    [ServiceContract]
    public interface IPathFinder
    {
        [OperationContract]
        int[] FindShortestPathBetween(int? startId, int? endId);
    }
}
