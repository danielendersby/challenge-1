﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NodeService
{
    [ServiceContract]
    public interface IAdminActions
    {
        [OperationContract]
        bool DropDataBase();

        [OperationContract]
        bool DropCollection();
    }
}
