﻿using MongoDB.Driver;
using MongoWrapperHelper;
using NodeService.AdminActionsBackend;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NodeService
{
    public class AdminActions : IAdminActions
    {
        public bool DropDataBase()
        {
            //TODO: Implement Authentication

            var dropper = new Dropper();
            return dropper.DropDataBase();
        }

        public bool DropCollection()
        {
            //TODO: Implement Authentication

            var dropper = new Dropper();
            return dropper.DropCollection();
        }
    }
}
