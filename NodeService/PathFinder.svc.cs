﻿using NodeService.PathFinderBackend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NodeService
{
    public class PathFinder : IPathFinder
    {
        public int[] FindShortestPathBetween(int? startId, int? endId)
        {
            if (!startId.HasValue)
                throw new ArgumentException("No Start Node ID provided");

            if (!endId.HasValue)
                throw new ArgumentException("No End Node ID provided");

            //TODO: Implement Authentication

            var pathGetter = new PathGetter();
            return pathGetter.GetShortestPathBetween(startId.Value, endId.Value);
        }
    }
}
