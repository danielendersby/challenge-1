﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoWrapperHelper;
using NodeService.Other;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NodeService.DataManagerBackend
{
    public class Upserter
    {
        private MongoWrapper mongoWrapper;

        public Upserter()
        {
            mongoWrapper = new MongoWrapper();
        }

        public bool UpsertNode(Node node)
        {
            try
            {
                var nodeCollection = mongoWrapper.GetCollection<Node>(ConfigHelper.NodeDatabase, ConfigHelper.NodeCollection);

                upsetNode(node, nodeCollection);
            }
            catch (ConfigurationErrorsException e)
            {
                throw e;
            }
            catch (MongoWrapperException e)
            {
                throw e;
            }
            catch (MongoException e)
            {
                throw e;
            }
            catch //(Exception e) Unknown Exception not exposed to user for security
            {
                throw new Exception("Unknwon Error occored Upseting Node");
            }

            return true;
        }

        public bool UpsertNodes(IEnumerable<Node> nodes)
        {
            try
            {
                var nodeCollection = mongoWrapper.GetCollection<Node>(ConfigHelper.NodeDatabase, ConfigHelper.NodeCollection);

                foreach (var node in nodes)
                {
                    upsetNode(node, nodeCollection);
                }

            }
            catch (ConfigurationErrorsException e)
            {
                throw e;
            }
            catch (MongoWrapperException e)
            {
                throw e;
            }
            catch (MongoException e)
            {
                throw e;
            }
            catch //(Exception e) Unknown Exception not exposed to user for security
            {
                throw new Exception("Unknwon Error occored Upseting Nodes");
            }

            return true;
        }

        private void upsetNode(Node node, IMongoCollection<Node> collection)
        {
            try
            {
                collection.ReplaceOne(
                    n => n.ID == node.ID,
                    node,
                    new UpdateOptions
                    {
                        IsUpsert = true, //Inset if new
                    });
            }
            catch
            {
                throw new MongoException(string.Format("Unable to Upset Node with ID '{0}'", node.ID));
            }
        }
    }
}