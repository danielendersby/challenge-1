﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoWrapperHelper;
using NodeService.Other;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NodeService.DataManagerBackend
{
    public class Remover
    {
        private MongoWrapper mongoWrapper;

        public Remover()
        {
            mongoWrapper = new MongoWrapper();
        }

        public bool RemoveNode(Node node)
        {
            return RemoveNode(node.ID);
        }

        public bool RemoveNode(int id)
        {
            try
            {
                var nodeCollection = mongoWrapper.GetCollection<Node>(ConfigHelper.NodeDatabase, ConfigHelper.NodeCollection);

                removeNodes(new int[] { id }, nodeCollection);
            }
            catch (ConfigurationErrorsException e)
            {
                throw e;
            }
            catch (MongoWrapperException e)
            {
                throw e;
            }
            catch (MongoException e)
            {
                throw e;
            }
            catch //(Exception e)
            {
                throw new Exception("Unknwon Error occored Removing Node");
            }

            return true;
        }

        public bool RemoveNodes(IEnumerable<int> ids)
        {
            try
            {
                var nodeCollection = mongoWrapper.GetCollection<Node>(ConfigHelper.NodeDatabase, ConfigHelper.NodeCollection);

                removeNodes(ids, nodeCollection);
            }
            catch (ConfigurationErrorsException e)
            {
                throw e;
            }
            catch (MongoWrapperException e)
            {
                throw e;
            }
            catch (MongoException e)
            {
                throw e;
            }
            catch //(Exception e)
            {
                throw new Exception("Unknwon Error occored Removing Node");
            }

            return true;
        }

        private void removeNodes(IEnumerable<int> ids, IMongoCollection<Node> collection)
        {
            try
            {
                collection.DeleteMany(n => ids.Contains(n.ID));
            }
            catch
            {
                throw new MongoException("Unable to Remove Nodes");
            }
        }
    }
}