﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoWrapperHelper;
using NodeService.Other;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NodeService.DataManagerBackend
{
    public class Getter
    {
        private MongoWrapper mongoWrapper;

        public Getter()
        {
            mongoWrapper = new MongoWrapper();
        }

        public IEnumerable<Node> GetAllNodes()
        {
            try
            {
                var nodeCollection = mongoWrapper.GetCollection<Node>(ConfigHelper.NodeDatabase, ConfigHelper.NodeCollection);

                return nodeCollection.
                Find(n => true).ToList();
            }
            catch (ConfigurationErrorsException e)
            {
                throw e;
            }
            catch (MongoWrapperException e)
            {
                throw e;
            }
            catch (MongoException e)
            {
                throw e;
            }
            catch //(Exception e)
            {
                throw new Exception("Unknwon Error occored Getting All Nodes");
            }
        }

        public Node GetNodeWith(int id)
        {
            return GetAllNodesWith(new int[] { id }).First();
        }

        public IEnumerable<Node> GetAllNodesWith(IEnumerable<int> ids)
        {
            try
            {
                var nodeCollection = mongoWrapper.GetCollection<Node>(ConfigHelper.NodeDatabase, ConfigHelper.NodeCollection);

                return getNodes(ids, nodeCollection);
            }
            catch (ConfigurationErrorsException e)
            {
                throw e;
            }
            catch (MongoWrapperException e)
            {
                throw e;
            }
            catch (MongoException e)
            {
                throw e;
            }
            catch //(Exception e)
            {
                throw new Exception("Unknwon Error occored Getting Nodes With ID");
            }
        }

        private List<Node> getNodes(IEnumerable<int> ids, IMongoCollection<Node> collection)
        {
            try
            {
                return collection.Find(n => ids.Contains(n.ID)).ToList();
            }
            catch
            {
                throw new MongoException("Unable to Find Nodes");
            }
        }
    }
}