﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoWrapperHelper;
using NodeService.Other;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NodeService.DataManagerBackend
{
    public class Trimmer
    {
        private MongoWrapper mongoWrapper;

        public Trimmer()
        {
            mongoWrapper = new MongoWrapper();
        }

        public bool TrimUnreferencedNodes(IEnumerable<int> ids)
        {
            try
            {
                var nodeCollection = mongoWrapper.GetCollection<Node>(ConfigHelper.NodeDatabase, ConfigHelper.NodeCollection);

                nodeCollection.DeleteMany(n => !ids.Contains(n.ID));
            }
            catch (ConfigurationErrorsException e)
            {
                throw e;
            }
            catch (MongoWrapperException e)
            {
                throw e;
            }
            catch (MongoException e)
            {
                throw e;
            }
            catch //(Exception e)
            {
                throw new Exception("Unknwon Error Trimming Unreferenced Nodes");
            }

            return true;
        }
    }
}