﻿using MongoDB.Bson.Serialization.Attributes;
using MongoWrapperHelper;
using NodeService.DataManagerBackend;
using NodeService.Other;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Web;

namespace NodeService.PathFinderBackend
{
    public class PathLink
    {
        [BsonId]
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int[] AdjacentNodes { get; set; }
    }

    public class PathGetter
    {
        public int[] GetShortestPathBetween(int startId, int endId)
        {
            if (startId == endId)
                return new int[] { startId, endId };

            var mongoWrapper = new MongoWrapper();
            var nodeCollection = mongoWrapper.GetCollection<Node>(ConfigHelper.NodeDatabase, ConfigHelper.NodeCollection);

            //I atempted to use Map Reduce to create a list of all possible bi directioanl paths 
            //My idea was to emit the documents ID with each of the adjacent nodes id as both key and pair
            //For Example emit(this.id, adjacentID); emit(adjacentID, this.id);
            //I thought this would then reduce to a single key and all possible bi directional paths
            //This could then be used to search though

            //A better solution would have been to use a graph database such as Neo4j but I chose to use MongoDB as it could provide more options if the project was taken further and because it's FOSS software

            //I have left this here for review

            //var map = new BsonJavaScript(@"
            //function() {
            //	var node = this;

            //	node.AdjacentNodes.forEach(function(value)
            //                {   
            //                    if(node.ID != value)
            //                    {           
            //		    emit(node.ID, value);
            //		    emit(value, node.ID);
            //                    }
            //                }); 
            //            }");

            //        //if(key != null && key.ID != null)
            //        var reduce = new BsonJavaScript(@"        
            //function(key, values) 
            //            {
            //                if(key != null)
            //                {					
            //                    var result = {ID: key, AdjacentNodes: [1, 2] };
            //	    return result;
            //                }
            //}");

            //var mapReduceOptions = new MapReduceOptions<Node, SimpleReduceResult<PathLink>>()
            //{
            //    Verbose = true
            //};

            //var mapReduceResult = nodeCollection.MapReduce<PathLink>(map, reduce);

            //var pathLinkAsList = mapReduceResult.ToList();
            //var firstNode = pathLinkAsList.Select(p => p.ID == startId);

            //if (firstNode == null)
            //Node does not exist, return null

            var getter = new Getter();
            var allNodes = getter.GetAllNodes();
            var allPathWrapperNodes = allNodes.Select(n => n as NodePathWrapper);

            //Parallel.ForEach(allPathWrapperNodes, (pathWrappedNode) =>
            //{
            //    pathWrappedNode.CreateAdjacentNodeLinks(allPathWrapperNodes);
            //});

            var startNode = allPathWrapperNodes.First(n => n.ID == startId);
            if (startNode == null)
            {
                //Start ID does not exist in collection
                return null;
            }

            var endNode = allPathWrapperNodes.First(n => n.ID == endId);
            if (endNode == null)
            {
                //End ID does not exist in collection
                return null;
            }

            return null;
        }
    }
}