﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NodeService.PathFinderBackend
{
    public class NodePathWrapper : Node
    {
        public List<NodePathWrapper> AdjacentNodePathWrapperLinks { get; private set; }

        public NodePathWrapper()
            : base()
        {
            AdjacentNodePathWrapperLinks = new List<NodePathWrapper>();
        }

        public void CreateAdjacentNodeLinks(IEnumerable<NodePathWrapper> allNodePathWrappers)
        {
            if (AdjacentNodePathWrapperLinks == null)
                AdjacentNodePathWrapperLinks = new List<NodePathWrapper>();

            foreach (var adjacentNodeID in AdjacentNodes)
            {
                var foundNode = allNodePathWrappers.First(n => n.ID == adjacentNodeID);

                if (foundNode != null)
                    AdjacentNodePathWrapperLinks.Add(foundNode);
            }
        }
    }
}