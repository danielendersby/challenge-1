# README #

Daniel Endersby Perform OTT Challenge 1.

# Prerequisites and Config #

This solution uses MongoDB as it's database.
The user will need to have a MongoDB instance running localy or on an external server. To install this please consult MongoDB's website.
To configure this enter a valid MongoDB connection string in the NodeService project's "Web.Config" in the "MongoDBConnectionString" section.
The user also has the ability to configure the DataBase and Collection name used here under NodeDataBase and NodeCollection sections respectively.

The NodeDataImporter can import Nodes from a default file path, this can be changes in it's "App.Config" in the "LoadDirectory" section.

# Tasks #
WCF Service:
The DataManager can Get Nodes, Insert or Update (Upsert) Nodes, Remove Nodes and Trim Nodes not in a provided list of IDs. The Trim action is used to remove Nodes not included in a new Data Import without having to drop the collection.
The AdminActions can Drop a Collection or Database.
The PathFinder was not implemented. I attempted to MapReduce a list of all possible paths between nodes to convert the unidirectional data to bidirectional that could then be searched through but was unable to parse my MapReduce result in time. There is left over code here where I started to implemented the same idea within C# but ran out of time to complete it. 
The WCF service imports a reusable MongoDb wrapper to simplify getting databases and collection.

Console Data Importer:
The Data Importer can Drop the Database or Collection and import Nodes in XML format from a config defined or user entered path. 
The Node Importer will ignore Nodes that are invalid and inform the user they have failed to parse but not why, this would be a good usability improvement.
When importing Nodes the user is given the option to Trim Nodes in the Collection that are not being imported now but defaults to no.

WPF UI:
The UI displays all the Nodes from the Collection in a uniform grid and will use scroll bars where needed.
The UI does not draw the connecting links between nodes but does calculate what view models to link up without overdraw. This collection of links is unidirectional to prevent overdraw so should not be used for pathing.
The calculate shortest path button is not implemented.

# Finally #

I'm pleased with the WCF Service and Console Data Importer but regret I did not leave more time to work on the WPF UI as ultimately this is the user’s first impression.