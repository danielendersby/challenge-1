﻿using NodeUI.DataManagerService;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeUI.ViewModels
{
    class NodeViewModel : INotifyPropertyChanged
    {
        private Node node;

        public int ID
        {
            get
            {
                return node.ID;
            }
        }

        public string Label
        {
            get
            {
                return node.Label;
            }
        }

        public ObservableCollection<NodeViewModel> AdjacentViewModelNodesToConnect { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public NodeViewModel(Node node)
        {
            AdjacentViewModelNodesToConnect = new ObservableCollection<NodeViewModel>();
            this.node = node;
        }

        public void CaculateAdjacentViewModelNodes(IEnumerable<NodeViewModel> allNodeViewModels)
        {
            //This collection only has single direction lines to save redraw on the UI as these links are not used for patthing
            var nodeViewModelsToAdd = allNodeViewModels
                .Where(inNode => node.AdjacentNodes.Contains(inNode.ID))
                .Where(possibleToAdd => !possibleToAdd.AdjacentViewModelNodesToConnect.Contains(this));

            foreach (var toAdd in nodeViewModelsToAdd)
            {
                AdjacentViewModelNodesToConnect.Add(toAdd);
            }
        }
    }
}