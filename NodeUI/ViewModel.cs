﻿using NodeUI.DataManagerService;
using NodeUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NodeUI
{
    class ViewModel : INotifyPropertyChanged
    {
        private string informationText;
        public string InformationText
        {
            get
            {
                return informationText;
            }
            set
            {
                informationText = value;
                OnPropertyChanged("InformationText");
            }
        }

        private ObservableCollection<NodeViewModel> nodes = new ObservableCollection<NodeViewModel>();
        public ObservableCollection<NodeViewModel> Nodes { get { return nodes; } }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public ViewModel()
        {

        }

        public async void LoadNodes()
        {
            InformationText = "Loading All Nodes";

            Node[] allNodes = null;
            bool nodesLoaded = true;
            try
            {
                var dataManagerClient = new DataManagerClient();
                allNodes = await dataManagerClient.GetAllNodesAsync();
                nodesLoaded = true;
            }
            catch //(Exception e)
            {
                MessageBoxResult result = MessageBox.Show("Unable to Get All Nodes From Mongo DB",
                    "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            if (nodesLoaded)
            {
                if (allNodes != null && allNodes.Count() > 0)
                {
                    //Load Data Nodes into View Model Nodes
                    foreach (var node in allNodes)
                    {
                        nodes.Add(new NodeViewModel(node));
                    }

                    foreach(var node in nodes)
                    {
                        node.CaculateAdjacentViewModelNodes(nodes);
                    }

                    InformationText = string.Format("Loaded {0} Node(s)", nodes.Count);
                }
                else
                {
                    InformationText = "No Nodes Loaded";
                    MessageBoxResult result = MessageBox.Show("No Nodes recived from DataBase",
                        "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
    }
}