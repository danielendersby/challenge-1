﻿using NodeDataImporter.AdminActionsService;
using NodeDataImporter.DataManagerService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace NodeDataImporter
{
    class Program
    {
        static void Main(string[] args)
        {
            var mainLoop = true;

            Console.WriteLine("Node Data Loader");

            while (mainLoop)
            {
                Console.WriteLine("");
                Console.WriteLine("1) Drop Database");
                Console.WriteLine("2) Drop Nodee Collection");
                Console.WriteLine("3) Import and Update Nodes from xml in LoadDirectory");
                Console.WriteLine("4) Import and Update Nodes from xml in user defined load directory");
                Console.WriteLine("5) Exit");
                Console.Write("Please select an option: ");

                switch (Console.ReadLine()?.ToLower())
                {
                    case "1":
                        dropDatabase();

                        break;
                    case "2":
                        dropCollection();

                        break;
                    case "3":
                        string loadDirectory = ConfigurationManager.AppSettings["LoadDirectory"];

                        if (Directory.Exists(loadDirectory))
                            ImportAndUpdateNodesFromXml(loadDirectory);
                        else
                            Console.WriteLine("Load Directory from App.Config is invalid: {0}", loadDirectory);
                        break;

                    case "4":
                        Console.Write("Please enter Load Directory: ");
                        string userLoadDirectory = Console.ReadLine();

                        if (Directory.Exists(userLoadDirectory))
                            ImportAndUpdateNodesFromXml(userLoadDirectory);
                        else
                            Console.WriteLine("Provided Load Directory is invalid: {0}", userLoadDirectory);
                        break;

                    case "":
                    case "5":
                    case "e":
                    case "exit":
                        mainLoop = false;
                        break;
                    default:
                        Console.WriteLine("Unknown Input, please retry");
                        break;
                }
            }
        }

        private static void dropDatabase()
        {
            bool inputLoop = true;

            do
            {
                Console.Write("Drop Database? [y//N] ");

                switch (Console.ReadLine()?.ToLower())
                {
                    case "":
                    case "n":
                    case "no":
                        inputLoop = false;
                        break;

                    case "y":
                    case "yes":
                        try
                        {
                            var adminActions = new AdminActionsClient();
                            var result = adminActions.DropDataBase();

                            if (result)
                                Console.WriteLine("Database dropped");
                            else
                                Console.WriteLine("Failed to drop Database");

                            inputLoop = false;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Unable to Drop DataBase. Error {0}", e.Message);
                        }

                        inputLoop = false;
                        break;
                }
            }
            while (inputLoop);
        }

        private static void dropCollection()
        {
            bool inputLoop = true;

            do
            {
                Console.Write("Drop Collection in Database ? [y//N] ");

                switch (Console.ReadLine()?.ToLower())
                {
                    case "":
                    case "n":
                    case "no":
                        inputLoop = false;
                        break;

                    case "y":
                    case "yes":
                        try
                        {
                            var adminActions = new AdminActionsClient();
                            var result = adminActions.DropCollection();

                            if (result)
                                Console.WriteLine("Collection dropped from Database");
                            else
                                Console.WriteLine("Failed to drop Collection from Database");

                            inputLoop = false;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Unable to Drop Collection from DataBase. Error {0}", e.Message);
                        }
                        break;
                }
            }
            while (inputLoop);
        }

        private static void ImportAndUpdateNodesFromXml(string directory)
        {
            var nodeList = new List<Node>();

            foreach (var xmlFilePath in Directory.GetFiles(directory, "*.xml"))
            {
                XDocument xmlFile = null;

                try
                {
                    xmlFile = XDocument.Load(xmlFilePath);
                }
                catch (Exception e)
                {
                    Console.WriteLine("ImportAndUpdateNodesFromXml: Unable to load XML: {0}. Exception: ", xmlFilePath, e.Message);
                }

                if (string.Equals(xmlFile.Root.Name.LocalName, "node", StringComparison.OrdinalIgnoreCase))
                {
                    var node = NodeFactory.CreateFrom(xmlFile.Root);

                    if (node != null)
                        nodeList.Add(node);
                    else
                        Console.WriteLine("Unable to Parse Node file '{0}', ignored", Path.GetFileName(xmlFilePath));
                }
            }

            bool inputLoop = true;
            bool trimNodes = false;
            do
            {
                Console.Write("Delete Nodes in Database Collection that are not imported from XMl? [y//N] ");

                switch (Console.ReadLine()?.ToLower())
                {
                    case "":
                    case "n":
                    case "no":
                        inputLoop = false;
                        break;

                    case "y":
                    case "yes":
                        try
                        {
                            inputLoop = false;
                            trimNodes = true;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Unable to Drop Collection from DataBase. Error {0}", e.Message);
                        }
                        break;
                }
            }
            while (inputLoop);

            try
            {
                var dataManagerService = new DataManagerClient();

                //Remove all Documents in DataBase Collection that are not included new Node Set
                if (trimNodes)
                    dataManagerService.TrimUnreferencedNodes(nodeList.Select(n => n.ID).ToArray());
                //Insert and Update new Node Set
                var result = dataManagerService.UpsertNodes(nodeList.ToArray());

                if (result)
                    Console.WriteLine("Import or Updated {0} Nodes", nodeList.Count);
                else
                    Console.WriteLine("Failed to Import or Updated {0} Nodes. Integrity of data within collection unknown", nodeList.Count);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unable to Import and Update Nodes from XML. Error :{0}", e.Message);
            }
        }
    }
}
