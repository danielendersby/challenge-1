﻿using NodeDataImporter.DataManagerService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace NodeDataImporter
{
    public static class NodeFactory
    {
        public static Node CreateFrom(XElement nodeXmlElement)
        {
            if (nodeXmlElement == null)
                throw new ArgumentException("NodeFactory, CreateFrom XML Element: XML Element must be provided");

            if (!string.Equals(nodeXmlElement.Name.LocalName, "node", StringComparison.OrdinalIgnoreCase))
                throw new ArgumentException("NodeFactory, CreateFrom XML Element: XML Element is expected to be at Node level");

            bool isValid = true;

            int parsedID;
            string labelParsed = null;

            if (!int.TryParse(nodeXmlElement.Element("id")?.Value, out parsedID))
                isValid = false;
            else if ((labelParsed = nodeXmlElement.Element("label")?.Value) == null)
                isValid = false;

            int[] adjacentNodes = null;
            var adjacentNodesDescendant = nodeXmlElement.Descendants("adjacentNodes");
            if (adjacentNodesDescendant != null)
            {
                try
                {
                    adjacentNodes = (from adjacentID in adjacentNodesDescendant?.Elements("id")
                                     let parsedId = intTryParse(adjacentID.Value)
                                     where parsedId.HasValue
                                     select parsedId.Value).ToArray();
                }
                catch (ArgumentException e)
                {
                    isValid = false;
                }
            }
            else
            {
                isValid = false;
            }


            if (isValid)
            {
                return new Node()
                {
                    ID = parsedID,
                    Label = labelParsed,
                    AdjacentNodes = adjacentNodes,
                };
            }
            else
            {
                return null;
            }
        }

        private static int? intTryParse(string value)
        {
            int parsedValue = 0;
            if (int.TryParse(value, out parsedValue))
                return parsedValue;
            else
                throw new ArgumentException("Adjacent Node Values must be numerical");
        }
    }
}
